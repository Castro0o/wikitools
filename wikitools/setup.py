from setuptools import setup

setup(
    name='wikitools',
    version='0.0.1',
    packages=[''],
    url='https://gitlab.com/Castro0o/wikitools',
    license='GPLv3',
    author='Andre Castro',
    author_email='andre@artserve.org',
    description='A series of Python script to perform actions in Mediawiki instances '
)
