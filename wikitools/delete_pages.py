import argparse
import yaml
from pathlib import Path
from login.login import site

p = argparse.ArgumentParser(description='Script for mass page deletion'
                                        'based on page-list: '
                                        'pages/delete_pages.yaml',
                            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

p.add_argument('--dry', '-d', action='store_true',
               help='dry-run: no changes saved to the wiki')
args = p.parse_args()
print(args)

# read templates and yaml
pwd = Path('.')
project_dir = pwd.parent.absolute()
pages_f = project_dir / 'property_pages' / 'delete_pages.yml'
with open(pages_f, 'r') as p:
    page_list = yaml.load(p.read(), Loader=yaml.BaseLoader)

# delete pages
for pagename in page_list:
    print(f'\n** Deleting page: {pagename} **')
    page = site.pages[pagename]
    if page.exists and not args.dry:
        page.delete()
