import argparse
import yaml
from pathlib import Path
from jinja2 import Template
from login.login import site

p = argparse.ArgumentParser(description='Script for mass Property page creation'
                                        'based on page-list: pages/pages.yaml'
                                        ' & page template: pages/page.jinja',
                            formatter_class=argparse.ArgumentDefaultsHelpFormatter)
p.add_argument('-o', '--overwrite', action='store_true',
               help="When invoked pages will overwritten."
                    "Default behavior is to append content")
p.add_argument('-ns', '--namespace', required=True,
               choices=['dcterms', 'foaf', 'dc'],
               help="The namespace of the property."
                    "Usually no NS is required, but for SMW imported "
                    "ontologies the NS of the ontology is required"
                    "(require: True)")
p.add_argument('-uri', '--uri',  required=True,
               choices=['http://purl.org/dc/terms/',
                        'http://xmlns.com/foaf/0.1/',
                        'http://purl.org/dc/elements/1.1/'],
               help="The Universal Resource Identifier of the ontoglogy. "
                    "Example -uri http://purl.org/dc/terms/ "
                    "NOTE: URI MUST END WITH / (require: True)")

p.add_argument('--dry', '-d', action='store_true',
               help='dry-run: no changes saved to the wiki')
args = p.parse_args()
print(args)

# read templates and yaml
pwd = Path('.')
project_dir = pwd.parent.absolute()
pages_f = project_dir / 'property_pages' / f'pages_{args.namespace}.yml'
page_tmplt_f = project_dir / 'property_pages' / 'propertypage.jinja'
with open(pages_f, 'r') as p:
    page_list = yaml.load(p.read(), Loader=yaml.BaseLoader)
with open(page_tmplt_f, 'r') as tmplt:
    page_tmplt = Template(source=tmplt.read())

# create pages
for entry in page_list:
    entry_list = entry.split('|')
    pagename = f'Property:{entry_list[0]}'
    property_type = entry_list[1]
    prop = f'{ pagename.split(":")[-1] }'
    new_page_content = page_tmplt.render(prop_ns=args.namespace,
                                         uri=args.uri,
                                         prop=prop)
    print(f'\n** Editing page: {pagename} **')
    page = site.pages[pagename]

    if page.exists and args.overwrite is False:
        text = page.text()
        print(f'Existing Page Text:\n{text}')
        text += new_page_content
        print(f'New Page Text:\n{text}')
    else:
        # for new page and overwrite
        text = new_page_content
        print(f'Page Text:\n{text}')

    if args.dry == False:
        page.save(text)
