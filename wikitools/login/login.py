import yaml
from pathlib import Path
from mwclient import Site

pwd = Path('.')
project_dir = pwd.parent.absolute()
details_path = project_dir / 'wikidetails.yml'

with open(details_path, 'r') as details_f:
    details = yaml.load(details_f.read(), Loader=yaml.BaseLoader)

site = Site(host=details['host'],
            path=details['path'],
            scheme=details['scheme'])
site.login(username=details['user'], password=details['password'])

