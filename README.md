# Wiki Tools - python scripts to perform actions and interact with Mediawiki instances

## Requirements
* [mwclient](https://mwclient.readthedocs.io/en/latest/index.html) 
* [pyYaml](https://github.com/yaml/pyyaml.org)
* [Jinja2](https://jinja.palletsprojects.com)

`pip install -r requirements.txt`

## RUN

**Create property pages for imported ontology:**
* requeries:
    * list of propeties for NS in `property_pages/pages_NS.yml`
 * runs: `python property_create_ontology.py  --dry -ns dcterms -uri http://purl.org/dc/terms/ -o`
 
 ## TODO
 when creating the propety pages of imported ontogies **single out those which are classes** and add them as **categories**